const gifwrap = require('gifwrap');
const Jimp = require('jimp');

gifwrap.GifUtil.read("sonic.gif").then(inputGif => {
	console.log(inputGif.frames.length + " frames");
	inputGif.frames.forEach(frame => {
		console.log(frame);
		return;
		Jimp.read(frame.bitmap.data).then(img => {
			console.log(img.bitmap.width + "x" + img.bitmap.height);
		});
	});
});
