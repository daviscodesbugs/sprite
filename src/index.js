import 'jimp/browser/lib/jimp';

var app = new Vue({
	el: '#body-container',
	data: {
		color_index: {
			id: {},
			rgba: {}
		},
		encoded_image: [],
	},
	methods: {
		getImage: function () {
			var that = this;
			var x = document.getElementById("image-file");
			x.disabled = true;

			var fr = new FileReader();
			var file_data = null;
			fr.onload = function () {
				file_data = fr.result;
				Jimp.read(fr.result, (err, img) => {
					if (err) throw err;
					console.log(img.bitmap.width + "x" + img.bitmap.height);

					let new_img = [];
					for (let y = 0; y < img.bitmap.height; y++) {
						let new_row = [];
						for (let x = 0; x < img.bitmap.width; x++) {
							let rgba = Jimp.intToRGBA(img.getPixelColor(x, y));
							new_row.push(toRGBAString(rgba));
						}
						new_img.push(new_row);
					}

					that.color_index = createColorIndex(new_img);
					that.encoded_image = encodeImage(new_img, that.color_index);
					drawEncodedImage(that.encoded_image, that.color_index);
				});
			};
			fr.readAsDataURL(x.files[0]);
		},
		redrawImage: function () {
			var that = this;
			var element = document.getElementById("sprite");
			element.parentNode.removeChild(element);
			drawEncodedImage(that.encoded_image, that.color_index);
		}
	}
});

function createColorIndex(img) {
	var height = img.length;
	var width = img[0].length;

	var colors = {
		count: 0,
		rgba: {},
		id: {}
	};
	for (let y = 0; y < height; y++) {
		for (let x = 0; x < width; x++) {
			var pixel = img[y][x];
			if (colors.rgba[pixel] == null) {
				colors.rgba[pixel] = colors.count;
				colors.id[colors.count] = pixel;
				colors.count += 1;
			}
		}
	}
	return colors;
}

function drawEncodedImage(rows, color_index) {
	var new_canvas = document.createElement('canvas');
	new_canvas.setAttribute("height", rows.length * 10);
	new_canvas.setAttribute("width", rows[0].length * 10);
	new_canvas.setAttribute("id", "sprite");
	document.getElementById("canvas-container").appendChild(new_canvas);

	var c = document.getElementById("sprite");
	var ctx = c.getContext("2d");

	for (let i = 0; i < rows.length; i++) {
		drawEncodedRowByPixel(i, rows[i], color_index, ctx);
	}
}

function drawEncodedRowByPixel(index, pixels, color_index, ctx) {
	var row_offset = index * 10;
	var col_offset = 0;
	var width = 10;

	pixels.forEach(function (pixel) {
		ctx.fillStyle = color_index.id[pixel];
		ctx.fillRect(col_offset, row_offset, width, 10);
		col_offset += width;
	});
}

function toRGBAString(rgba) {
	return "rgba(" + rgba.r + "," + rgba.g + "," + rgba.b + "," + (rgba.a / 255) + ")";
}

function encodeImage(img, color_index) {
	var height = img.length;
	var width = img[0].length;

	let new_img = [];
	for (let y = 0; y < height; y++) {
		let new_row = [];
		for (let x = 0; x < width; x++) {
			var pixel = img[y][x];
			new_row.push(color_index.rgba[pixel]);
		}
		new_img.push(new_row);
	}
	return new_img;
}