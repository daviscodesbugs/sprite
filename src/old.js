

function setCanvasDimensions(width, height) {
    console.log("Setting dimensions");
    app.canvasWidth = width;
    app.canvasHeight = height;
}

function addIndicesToDOM(color_index) {
    var el_color_list = document.getElementById('color-list');
    for (var i = 0; i < color_index.count; i++) {
        el_color_list.innerHTML += "<li class='color_index' style='background-color: " + color_index.id[i] + "'>" + i + "</li>";
    }
}

function drawImage(rows) {
    for (let i = 0; i < rows.length; i++) {
        drawRowByPixel(i, rows[i]);
    }
}

function drawRow(index, row_content) {
    var row_offset = index * 10;
    var col_offset = 0;

    row_content.forEach(function (pixel_group) {
        if (!pixel_group[0]) {
            col_offset = pixel_group[1] * 10;
        } else {
            var width = pixel_group[1] * 10;
            ctx.fillStyle = pixel_group[0];
            ctx.fillRect(col_offset, row_offset, width, 10);
            col_offset += width;
        }
    });
}

function drawRowByPixel(index, pixels) {
    var row_offset = index * 10;
    var col_offset = 0;
    var width = 10;

    pixels.forEach(function (pixel) {
        ctx.fillStyle = pixel;
        ctx.fillRect(col_offset, row_offset, width, 10);
        col_offset += width;
    });
}

function getImage() {
    var x = document.getElementById("image-file");
    x.disabled = true;
    console.log(x.files);

    var fr = new FileReader();
    var file_data = null;
    fr.onload = function () {
        file_data = fr.result;
        Jimp.read(fr.result, (err, img) => {
            if (err) throw err;
            console.log(img.bitmap.width + "x" + img.bitmap.height);

            let new_img = [];
            for (let y = 0; y < img.bitmap.height; y++) {
                let new_row = [];
                for (let x = 0; x < img.bitmap.width; x++) {
                    let rgba = Jimp.intToRGBA(img.getPixelColor(x, y));
                    new_row.push(toRGBAString(rgba));
                }
                new_img.push(new_row);
            }
            let color_index = createColorIndex(new_img);
            let encoded_image = encodeImage(new_img, color_index);
            drawEncodedImage(encoded_image, color_index);
            addIndicesToDOM(color_index);
        });
    };
    fr.readAsDataURL(x.files[0]);
}