const Jimp = require('jimp');

Jimp.read(process.argv[2], (err, img) => {
  if (err) throw err;
  console.log(img.bitmap.width + "x" + img.bitmap.height);

  let new_img = [];
  for (let y = 0; y < img.bitmap.height; y++) {
    let new_row = [];
    for (let x = 0; x < img.bitmap.width; x++) {
      let rgba = Jimp.intToRGBA(img.getPixelColor(x, y));
      new_row.push(toRGBAString(rgba));
    }
    new_img.push(new_row);
  }
  console.log(new_img);

  let color_index = createColorIndex(new_img);
  console.log(color_index)
  let encoded_image = encodeImage(new_img, color_index);
  console.log(encoded_image);
});

function toRGBAString(rgba) {
  return "rgba(" + rgba.r + "," + rgba.g + "," + rgba.b + "," + (rgba.a / 255) + ")";
}

function createColorIndex(img) {
  var height = img.length;
  var width = img[0].length;

  var colors = {
    count: 0,
    rgba: {},
    id: {}
  };
  for (let y = 0; y < height; y++) {
    for (let x = 0; x < width; x++) {
      var pixel = img[y][x];
      if (colors.rgba[pixel] == null) {
        colors.rgba[pixel] = colors.count;
        colors.id[colors.count] = pixel;
        colors.count += 1;
      }
    }
  }
  return colors;
}

function encodeImage(img, color_index) {
  var height = img.length;
  var width = img[0].length;
  
  let new_img = [];
  for (let y = 0; y < height; y++) {
    let new_row = [];    
    for (let x = 0; x < width; x++) {
      var pixel = img[y][x];
      new_row.push(color_index.rgba[pixel]);
    }
    new_img.push(new_row);
  }
  return new_img;
}